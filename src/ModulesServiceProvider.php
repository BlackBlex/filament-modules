<?php

namespace Coolsam\FilamentModules;

use Coolsam\FilamentModules\Commands\ModuleMakePanelCommand;
use Coolsam\FilamentModules\Extensions\LaravelModulesServiceProvider;
use Filament\Facades\Filament;
use Illuminate\Support\Arr;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;
use Nwidart\Modules\Facades\Module;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ModulesServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('modules')
            ->hasConfigFile('modules')
            ->hasViews()
            ->hasCommands([
                ModuleMakePanelCommand::class,
            ]);
    }

    public function register()
    {
        $this->app->register(LaravelModulesServiceProvider::class);
        $this->app->singleton('coolsam-modules', Modules::class);
        $this->app->afterResolving('filament', function () {
            foreach (Filament::getPanels() as $panel) {
                $id = Str::of($panel->getId());
                if ($id->contains('::')) {
                    $moduleName = str_replace('admin/','', $panel->getPath());
                    $module = Module::find($moduleName);
                    $configPath = $module->getPath() . '/Config/config.php';
                    $config = require_once($configPath);
                    $titleTranslate = Arr::get($config, 'panel_title.admin', null);
                    $title = $id->replace(['::', '-'], [' ', ' '])->title()->toString();

                    $panel
                        ->renderHook(
                            'panels::sidebar.nav.start',
                            function () use($titleTranslate, $title) {
                                if ($titleTranslate) {
                                    return Blade::render('<h2 class="font-black text-xl">{{ __($title) }}</h2>', ['title' => $titleTranslate]);
                                } else {
                                    return Blade::render('<h2 class="font-black text-xl">{{ $title }}</h2>', ['title' => $title]);
                                }
                            },
                        )
                        ->renderHook(
                            'panels::sidebar.nav.end',
                            fn () => Blade::render('<x-filament::link :href="route(\'filament.admin.pages.dashboard\')" size="xl" color="gray" icon="forkawesome-angle-double-left" class="self-start" tooltip="">{{__("back")}}</x-filament::link>'),
                        );
                }
            }
        });

        return parent::register();
    }
}
